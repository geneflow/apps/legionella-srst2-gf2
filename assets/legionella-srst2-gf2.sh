#!/bin/bash

# Retrieves Legionella pneumophila in silico ST profile wrapper script


###############################################################################
#### Helper Functions ####
###############################################################################

## ****************************************************************************
## Usage description should match command line arguments defined below
usage () {
    echo "Usage: $(basename "$0")"
    echo "  --input => Input FASTQ File"
    echo "  --pair => Input FASTQ Pair"
    echo "  --mlst_db => MLST DB"
    echo "  --mlst_definitions => MLST Definitions"
    echo "  --forward => Forward Sequence Suffix"
    echo "  --reverse => Reverse Sequence Suffix"
    echo "  --mlst_max_mismatch => MLST Max Mismatch"
    echo "  --output => Output Directory"
    echo "  --output_basename => Output Basename"
    echo "  --exec_method => Execution method (singularity, environment, auto)"
    echo "  --exec_init => Execution initialization command(s)"
    echo "  --help => Display this help message"
}
## ****************************************************************************

# report error code for command
safeRunCommand() {
    cmd="$@"
    eval "$cmd; "'PIPESTAT=("${PIPESTATUS[@]}")'
    for i in ${!PIPESTAT[@]}; do
        if [ ${PIPESTAT[$i]} -ne 0 ]; then
            echo "Error when executing command #${i}: '${cmd}'"
            exit ${PIPESTAT[$i]}
        fi
    done
}

# print message and exit
fail() {
    msg="$@"
    echo "${msg}"
    usage
    exit 1
}

# always report exit code
reportExit() {
    rv=$?
    echo "Exit code: ${rv}"
    exit $rv
}

trap "reportExit" EXIT

# check if string contains another string
contains() {
    string="$1"
    substring="$2"

    if test "${string#*$substring}" != "$string"; then
        return 0    # $substring is not in $string
    else
        return 1    # $substring is in $string
    fi
}



###############################################################################
## SCRIPT_DIR: directory of current script, depends on execution
## environment, which may be detectable using environment variables
###############################################################################
if [ -z "${AGAVE_JOB_ID}" ]; then
    # not an agave job
    SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
else
    echo "Agave job detected"
    SCRIPT_DIR=$(pwd)
fi
## ****************************************************************************



###############################################################################
#### Parse Command-Line Arguments ####
###############################################################################

getopt --test > /dev/null
if [ $? -ne 4 ]; then
    echo "`getopt --test` failed in this environment."
    exit 1
fi

## ****************************************************************************
## Command line options should match usage description
OPTIONS=
LONGOPTIONS=help,exec_method:,exec_init:,input:,pair:,mlst_db:,mlst_definitions:,forward:,reverse:,mlst_max_mismatch:,output:,output_basename:,
## ****************************************************************************

# -temporarily store output to be able to check for errors
# -e.g. use "--options" parameter by name to activate quoting/enhanced mode
# -pass arguments only via   -- "$@"   to separate them correctly
PARSED=$(\
    getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@"\
)
if [ $? -ne 0 ]; then
    # e.g. $? == 1
    #  then getopt has complained about wrong arguments to stdout
    usage
    exit 2
fi

# read getopt's output this way to handle the quoting right:
eval set -- "$PARSED"

## ****************************************************************************
## Set any defaults for command line options
MLST_DB="${SCRIPT_DIR}/database/Profile_seq_20200624.fasta"
MLST_DEFINITIONS="${SCRIPT_DIR}/database/Profile_schema_final_20200624.txt"
FORWARD="_R1"
REVERSE="_R2"
MLST_MAX_MISMATCH="0"
OUTPUT_BASENAME="PRS_pipeline_SBT"
EXEC_METHOD="auto"
EXEC_INIT=":"
## ****************************************************************************

## ****************************************************************************
## Handle each command line option. Lower-case variables, e.g., ${file}, only
## exist if they are set as environment variables before script execution.
## Environment variables are used by Agave. If the environment variable is not
## set, the Upper-case variable, e.g., ${FILE}, is assigned from the command
## line parameter.
while true; do
    case "$1" in
        --help)
            usage
            exit 0
            ;;
        --input)
            if [ -z "${input}" ]; then
                INPUT=$2
            else
                INPUT="${input}"
            fi
            shift 2
            ;;
        --pair)
            if [ -z "${pair}" ]; then
                PAIR=$2
            else
                PAIR="${pair}"
            fi
            shift 2
            ;;
        --mlst_db)
            if [ -z "${mlst_db}" ]; then
                MLST_DB=$2
            else
                MLST_DB="${mlst_db}"
            fi
            shift 2
            ;;
        --mlst_definitions)
            if [ -z "${mlst_definitions}" ]; then
                MLST_DEFINITIONS=$2
            else
                MLST_DEFINITIONS="${mlst_definitions}"
            fi
            shift 2
            ;;
        --forward)
            if [ -z "${forward}" ]; then
                FORWARD=$2
            else
                FORWARD="${forward}"
            fi
            shift 2
            ;;
        --reverse)
            if [ -z "${reverse}" ]; then
                REVERSE=$2
            else
                REVERSE="${reverse}"
            fi
            shift 2
            ;;
        --mlst_max_mismatch)
            if [ -z "${mlst_max_mismatch}" ]; then
                MLST_MAX_MISMATCH=$2
            else
                MLST_MAX_MISMATCH="${mlst_max_mismatch}"
            fi
            shift 2
            ;;
        --output)
            if [ -z "${output}" ]; then
                OUTPUT=$2
            else
                OUTPUT="${output}"
            fi
            shift 2
            ;;
        --output_basename)
            if [ -z "${output_basename}" ]; then
                OUTPUT_BASENAME=$2
            else
                OUTPUT_BASENAME="${output_basename}"
            fi
            shift 2
            ;;
        --exec_method)
            if [ -z "${exec_method}" ]; then
                EXEC_METHOD=$2
            else
                EXEC_METHOD="${exec_method}"
            fi
            shift 2
            ;;
        --exec_init)
            if [ -z "${exec_init}" ]; then
                EXEC_INIT=$2
            else
                EXEC_INIT="${exec_init}"
            fi
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Invalid option"
            usage
            exit 3
            ;;
    esac
done
## ****************************************************************************

## ****************************************************************************
## Log any variables passed as inputs
echo "Input: ${INPUT}"
echo "Pair: ${PAIR}"
echo "Mlst_db: ${MLST_DB}"
echo "Mlst_definitions: ${MLST_DEFINITIONS}"
echo "Forward: ${FORWARD}"
echo "Reverse: ${REVERSE}"
echo "Mlst_max_mismatch: ${MLST_MAX_MISMATCH}"
echo "Output: ${OUTPUT}"
echo "Output_basename: ${OUTPUT_BASENAME}"
echo "Execution Method: ${EXEC_METHOD}"
echo "Execution Initialization: ${EXEC_INIT}"
## ****************************************************************************



###############################################################################
#### Validate and Set Variables ####
###############################################################################

## ****************************************************************************
## Add app-specific logic for handling and parsing inputs and parameters

# INPUT input

if [ -z "${INPUT}" ]; then
    echo "Input FASTQ File required"
    echo
    usage
    exit 1
fi
# make sure INPUT is staged
count=0
while [ ! -f "${INPUT}" ]
do
    echo "${INPUT} not staged, waiting..."
    sleep 1
    count=$((count+1))
    if [ $count == 10 ]; then break; fi
done
if [ ! -f "${INPUT}" ]; then
    echo "Input FASTQ File not found: ${INPUT}"
    exit 1
fi
INPUT_FULL=$(readlink -f "${INPUT}")
INPUT_DIR=$(dirname "${INPUT_FULL}")
INPUT_BASE=$(basename "${INPUT_FULL}")


# PAIR input

if [ -z "${PAIR}" ]; then
    echo "Input FASTQ Pair required"
    echo
    usage
    exit 1
fi
# make sure PAIR is staged
count=0
while [ ! -f "${PAIR}" ]
do
    echo "${PAIR} not staged, waiting..."
    sleep 1
    count=$((count+1))
    if [ $count == 10 ]; then break; fi
done
if [ ! -f "${PAIR}" ]; then
    echo "Input FASTQ Pair not found: ${PAIR}"
    exit 1
fi
PAIR_FULL=$(readlink -f "${PAIR}")
PAIR_DIR=$(dirname "${PAIR_FULL}")
PAIR_BASE=$(basename "${PAIR_FULL}")


# MLST_DB input

if [ -n "${MLST_DB}" ]; then
    # make sure ${MLST_DB} is staged
    count=0
    while [ ! -f "${MLST_DB}" ]
    do
        echo "${MLST_DB} not staged, waiting..."
        sleep 1
        count=$((count+1))
        if [ $count == 10 ]; then break; fi
    done
    if [ ! -f "${MLST_DB}" ]; then
        echo "MLST DB not found: ${MLST_DB}"
        exit 1
    fi
    MLST_DB_FULL=$(readlink -f "${MLST_DB}")
    MLST_DB_DIR=$(dirname "${MLST_DB_FULL}")
    MLST_DB_BASE=$(basename "${MLST_DB_FULL}")
fi



# MLST_DEFINITIONS input

if [ -n "${MLST_DEFINITIONS}" ]; then
    # make sure ${MLST_DEFINITIONS} is staged
    count=0
    while [ ! -f "${MLST_DEFINITIONS}" ]
    do
        echo "${MLST_DEFINITIONS} not staged, waiting..."
        sleep 1
        count=$((count+1))
        if [ $count == 10 ]; then break; fi
    done
    if [ ! -f "${MLST_DEFINITIONS}" ]; then
        echo "MLST Definitions not found: ${MLST_DEFINITIONS}"
        exit 1
    fi
    MLST_DEFINITIONS_FULL=$(readlink -f "${MLST_DEFINITIONS}")
    MLST_DEFINITIONS_DIR=$(dirname "${MLST_DEFINITIONS_FULL}")
    MLST_DEFINITIONS_BASE=$(basename "${MLST_DEFINITIONS_FULL}")
fi




# FORWARD parameter
if [ -n "${FORWARD}" ]; then
    :
else
    :
fi


# REVERSE parameter
if [ -n "${REVERSE}" ]; then
    :
else
    :
fi


# MLST_MAX_MISMATCH parameter
if [ -n "${MLST_MAX_MISMATCH}" ]; then
    :
else
    :
fi


# OUTPUT parameter
if [ -n "${OUTPUT}" ]; then
    :
    OUTPUT_FULL=$(readlink -f "${OUTPUT}")
    OUTPUT_DIR=$(dirname "${OUTPUT_FULL}")
    OUTPUT_BASE=$(basename "${OUTPUT_FULL}")
    LOG_FULL="${OUTPUT_DIR}/_log"
    TMP_FULL="${OUTPUT_DIR}/_tmp"
else
    :
    echo "Output Directory required"
    echo
    usage
    exit 1
fi


# OUTPUT_BASENAME parameter
if [ -n "${OUTPUT_BASENAME}" ]; then
    :
else
    :
fi

## ****************************************************************************

## EXEC_METHOD: execution method
## Suggested possible options:
##   auto: automatically determine execution method
##   singularity: singularity image packaged with the app
##   docker: docker containers from docker-hub
##   environment: binaries available in environment path

## ****************************************************************************
## List supported execution methods for this app (space delimited)
exec_methods="singularity environment auto"
## ****************************************************************************

## ****************************************************************************
# make sure the specified execution method is included in list
if ! contains " ${exec_methods} " " ${EXEC_METHOD} "; then
    echo "Invalid execution method: ${EXEC_METHOD}"
    echo
    usage
    exit 1
fi
## ****************************************************************************



###############################################################################
#### App Execution Initialization ####
###############################################################################

## ****************************************************************************
## Execute any "init" commands passed to the GeneFlow CLI
CMD="${EXEC_INIT}"
echo "CMD=${CMD}"
safeRunCommand "${CMD}"
## ****************************************************************************



###############################################################################
#### Auto-Detect Execution Method ####
###############################################################################

# assign to new variable in order to auto-detect after Agave
# substitution of EXEC_METHOD
AUTO_EXEC=${EXEC_METHOD}
## ****************************************************************************
## Add app-specific paths to detect the execution method.
if [ "${EXEC_METHOD}" = "auto" ]; then
    # detect execution method
    if command -v singularity >/dev/null 2>&1; then
        AUTO_EXEC=singularity
    elif command -v python >/dev/null 2>&1 && command -v bowtie2 >/dev/null 2>&1 && command -v samtools >/dev/null 2>&1; then
        AUTO_EXEC=environment
    else
        echo "Valid execution method not detected"
        echo
        usage
        exit 1
    fi
    echo "Detected Execution Method: ${AUTO_EXEC}"
fi
## ****************************************************************************



###############################################################################
#### App Execution Preparation, Common to all Exec Methods ####
###############################################################################

## ****************************************************************************
## Add logic to prepare environment for execution
MNT=""; ARG=""; CMD0="mkdir -p ${OUTPUT_FULL} ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
MNT=""; ARG=""; CMD0="mkdir -p ${LOG_FULL} ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
## ****************************************************************************



###############################################################################
#### App Execution, Specific to each Exec Method ####
###############################################################################

## ****************************************************************************
## Add logic to execute app
## There should be one case statement for each item in $exec_methods
case "${AUTO_EXEC}" in
    singularity)
        MNT=""; ARG=""; MNT="${MNT} -B "; MNT="${MNT}\"${SCRIPT_DIR}:/data1\""; ARG="${ARG} \"/data1/updatedSRST2.py\""; ARG="${ARG} --mlst_max_mismatch"; ARG="${ARG} \"${MLST_MAX_MISMATCH}\""; ARG="${ARG} --input_pe"; MNT="${MNT} -B "; MNT="${MNT}\"${INPUT_DIR}:/data3\""; ARG="${ARG} \"/data3/${INPUT_BASE}\""; MNT="${MNT} -B "; MNT="${MNT}\"${PAIR_DIR}:/data4\""; ARG="${ARG} \"/data4/${PAIR_BASE}\""; ARG="${ARG} --output"; MNT="${MNT} -B "; MNT="${MNT}\"${OUTPUT_DIR}:/data5\""; ARG="${ARG} \"/data5/${OUTPUT_BASE}/${OUTPUT_BASENAME}\""; ARG="${ARG} --forward"; ARG="${ARG} \"${FORWARD}\""; ARG="${ARG} --reverse"; ARG="${ARG} \"${REVERSE}\""; ARG="${ARG} --save_scores"; ARG="${ARG} --log"; ARG="${ARG} --mlst_db"; MNT="${MNT} -B "; MNT="${MNT}\"${MLST_DB_DIR}:/data10\""; ARG="${ARG} \"/data10/${MLST_DB_BASE}\""; ARG="${ARG} --mlst_definitions"; MNT="${MNT} -B "; MNT="${MNT}\"${MLST_DEFINITIONS_DIR}:/data11\""; ARG="${ARG} \"/data11/${MLST_DEFINITIONS_BASE}\""; CMD0="singularity -s exec ${MNT} docker://geneflow/legionella-srst2:python2.7.18--scipy--bowtie2.2.4--samtools0.1.18 python ${ARG}"; CMD0="${CMD0} >\"${LOG_FULL}/${OUTPUT_BASE}-srst.stdout\""; CMD0="${CMD0} 2>\"${LOG_FULL}/${OUTPUT_BASE}-srst.stderr\""; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
        ;;
    environment)
        MNT=""; ARG=""; ARG="${ARG} \"${SCRIPT_DIR}/updatedSRST2.py\""; ARG="${ARG} --mlst_max_mismatch"; ARG="${ARG} \"${MLST_MAX_MISMATCH}\""; ARG="${ARG} --input_pe"; ARG="${ARG} \"${INPUT_DIR}/${INPUT_BASE}\""; ARG="${ARG} \"${PAIR_DIR}/${PAIR_BASE}\""; ARG="${ARG} --output"; ARG="${ARG} \"${OUTPUT_FULL}/${OUTPUT_BASENAME}\""; ARG="${ARG} --forward"; ARG="${ARG} \"${FORWARD}\""; ARG="${ARG} --reverse"; ARG="${ARG} \"${REVERSE}\""; ARG="${ARG} --save_scores"; ARG="${ARG} --log"; ARG="${ARG} --mlst_db"; ARG="${ARG} \"${MLST_DB_FULL}\""; ARG="${ARG} --mlst_definitions"; ARG="${ARG} \"${MLST_DEFINITIONS_FULL}\""; CMD0="python ${ARG}"; CMD0="${CMD0} >\"${LOG_FULL}/${OUTPUT_BASE}-srst.stdout\""; CMD0="${CMD0} 2>\"${LOG_FULL}/${OUTPUT_BASE}-srst.stderr\""; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
        ;;
esac
## ****************************************************************************



###############################################################################
#### Cleanup, Common to All Exec Methods ####
###############################################################################

## ****************************************************************************
## Add logic to cleanup execution artifacts, if necessary
## ****************************************************************************

