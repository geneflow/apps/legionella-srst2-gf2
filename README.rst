Legionalla SRST2 GeneFlow App
=============================

Version: 0.2-01

This GeneFlow2 app wraps the custom Legionella SRST2 python script.

Inputs
------

1. input: Input FASTQ File

2. pair: Input FASTQ Pair

3. mlst_db: MLST database. Default: Profile_seq_20200624.fasta

4. mlst_definitions: MLST definitions. Default: Profile_schema_final_20200624.txt 

Parameters
----------

1. forward: Forward Sequence Suffix. Default: _R1

2. reverse: Reverse Sequence Suffix. Default: _R2

3. mlst_max_mismatch: MLST Max Mismatch. Default: 0

4. output: Output directory

5. output_basename: Output basename. Default: PRS_pipeline_SBT
